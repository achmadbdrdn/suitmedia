import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Layout from './components/layout';
import About from './pages/about';
import Ideas from './pages/ideas';
import Work from './pages/work';
import Career from './pages/career';
import Contact from './pages/contact';
import Service from './pages/service';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<Layout />}>
          <Route path="/" element={<Ideas />} />
          <Route path="/work" element={<Work />} />
          <Route path="/about" element={<About />} />
          <Route path="/career" element={<Career />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/service" element={<Service />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
