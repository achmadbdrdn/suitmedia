import { useState, useEffect, useRef } from 'react';

export const useScrollDirection = () => {
  const [scrollDir, setScrollDir] = useState('scroll down');
  const [scrollPosition, setScrollPosition] = useState(0);

  useEffect(() => {
    const threshold = 0;
    let lastScrollY = window.pageYOffset;
    let ticking = false;

    const updateScrollDir = () => {
      const scrollY = window.pageYOffset;

      if (Math.abs(scrollY - lastScrollY) < threshold) {
        ticking = false;
        return;
      }
      setScrollDir(scrollY > lastScrollY ? 'scroll down' : 'scroll up');
      lastScrollY = scrollY > 0 ? scrollY : 0;
      setScrollPosition(window.pageYOffset);
      ticking = false;
    };

    const onScroll = () => {
      if (!ticking) {
        window.requestAnimationFrame(updateScrollDir);
        ticking = true;
      }
    };

    window.addEventListener('scroll', onScroll);

    return () => {
      window.removeEventListener('scroll', onScroll);
    };
  }, [scrollDir]);

  return { scrollDir, scrollPosition };
};
