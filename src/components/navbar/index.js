import React from 'react';
import Logo from '../../assets/logo.png';
import Logo2 from '../../assets/logo-2.png';
import './navbar.css';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useScrollDirection } from '../../hooks/useScrollDirection';

const NAVBAR__LINK = [
  { title: 'Work', route: '/work' },
  { title: 'About', route: '/about' },
  { title: 'Service', route: '/service' },
  { title: 'Ideas', route: '/' },
  { title: 'Career', route: '/career' },
  { title: 'Contact', route: '/contact' },
];

const Navbar = () => {
  const { pathname } = useLocation();
  const { scrollDir, scrollPosition } = useScrollDirection();
  const isNotOnTopPage = scrollPosition > 50;
  const isScrollingTop = scrollDir === 'scroll up' && isNotOnTopPage;

  return (
    <nav
      className={`navbar__container ${isNotOnTopPage && 'navbar__hide'} ${
        isScrollingTop && 'navbar__fixed'
      }`}>
      <img src={Logo} alt="logo" />
      <ul className="navbar__actions">
        {NAVBAR__LINK.map(({ title, route }) => (
          <Link to={route} key={title}>
            <li className={pathname === route ? 'active__link' : ''}>
              {title}
            </li>
          </Link>
        ))}
      </ul>
    </nav>
  );
};

export default Navbar;
