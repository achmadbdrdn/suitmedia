import React from 'react';
import './card.css';
import dayjs from 'dayjs';

const Card = ({ data }) => {
  return (
    <div className="card__container">
      <div className="card__image">
        <img src={data?.medium_image[0].url} alt="cover" loading="lazy" />
      </div>
      <div className="card__content">
        <p>{dayjs(data.published_at).format('DD MMMM YYYY')}</p>
        <h3>{data.title}</h3>
      </div>
    </div>
  );
};

export default Card;
