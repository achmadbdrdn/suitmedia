import React from 'react';
import { Outlet, useLocation } from 'react-router-dom';
import Navbar from '../navbar';

const Layout = () => {
  return (
    <>
      <Navbar />
      <Outlet />
    </>
  );
};

export default Layout;
