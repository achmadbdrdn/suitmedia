import React, { useEffect, useState } from 'react';
import Banner from '../../assets/banner.jpeg';
import axios from 'axios';
import './ideas.css';
import Card from '../../components/card';
import Paginate from 'react-paginate';
import { useSearchParams } from 'react-router-dom';

export default function Ideas() {
  const [searchParams, setSearchParams] = useSearchParams();
  const [data, setData] = useState([]);
  const [meta, setMeta] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (searchParams.get('page[number]') === null) {
      searchParams.set('page[number]', 1);
    }
    if (searchParams.get('page[size]') === null) {
      searchParams.set('page[size]', 10);
    }
    if (searchParams.get('sort') === null) {
      searchParams.set('sort', 'published_at');
    }
    setSearchParams(searchParams);
  }, [searchParams, setSearchParams]);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const { data } = await axios.get(
          `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${searchParams.get(
            'page[number]'
          )}&page[size]=${searchParams.get(
            'page[size]'
          )}&append[]=small_image&append[]=medium_image&sort=-${searchParams.get(
            'sort'
          )}`
        );
        setData(data.data);
        setMeta(data.meta);
      } catch (error) {
        console.log(error);
      }
      setIsLoading(false);
    };
    fetchData();
  }, [searchParams, setSearchParams]);

  function handleOnClickPage(e) {
    searchParams.set('page[number]', e.selected + 1);
    setSearchParams(searchParams);
  }

  function handleSetPageSize(e) {
    searchParams.set('page[size]', e.target.value);
    setSearchParams(searchParams);
  }

  const renderBannerContainer = () => (
    <div
      className="banner__container"
      style={{
        backgroundImage: `url(${Banner})`,
      }}
    />
  );

  return (
    <>
      {renderBannerContainer()}
      <main className="ideas__container">
        {isLoading ? (
          <p>Loading...</p>
        ) : (
          <>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <p>
                Showing{' '}
                {meta.current_page * meta.per_page - (meta.per_page - 1)} -{' '}
                {Math.min(meta.current_page * meta.per_page, meta.total)} of{' '}
                {meta.total}
              </p>
              <div className="select__container">
                <label htmlFor="page-size">Show per page:</label>
                <select
                  name="page-size"
                  value={searchParams.get('page[size]')}
                  onChange={(e) => handleSetPageSize(e)}>
                  <option value={5}>5</option>
                  <option value={10}>10</option>
                  <option value={15}>15</option>
                  <option value={20}>20</option>
                </select>
                <label htmlFor="sort">Sort by:</label>
                <select name="sort">
                  <option value="published_at">Newest</option>
                </select>
              </div>
            </div>
            <div className="ideas__content">
              {data.map((data) => (
                <Card {...{ data }} key={data.id} />
              ))}
            </div>
            <Paginate
              previousLabel={'<'}
              nextLabel={'>'}
              pageCount={meta.last_page ?? 0}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              containerClassName={'pagination'}
              onPageChange={(e) => handleOnClickPage(e)}
            />
          </>
        )}
      </main>
      {renderBannerContainer()}
    </>
  );
}
